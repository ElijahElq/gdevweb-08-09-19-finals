window.onload = function () {
    // resize gamebox div
    window.gamebox = document.getElementById('gamebox') || container || window.document.body;
    window.gamebox.style.width = "100%";
    window.gamebox.style.height = "100%";

    // set background color to greyish
    window.document.body.style["background-color"] = "#333333";

    // remove any padding or margin
    document.body.style.padding = "0px";
    document.body.style.margin = "0px";

    // define phaser game
    const game = new Phaser.Game(window.gamebox.clientWidth || window.innerWidth, window.gamebox.clientHeight || window.innerHeight, Phaser.AUTO, window.gamebox); 
    window.game = game;

    // initialize game strings based on browser language
    game.strings = strings[checkLanguage ()];

    // check if mobile or desktop
    game.isMobile = isMobile.any () ? true : false;

    // add menu state
    game.state.add('preloader', new Preloader());

    game.state.add('gametitle', new GameTitle());
    // add game state
    game.state.add('game', new Game());



    


    // initialize preloader state
    game.state.start('preloader');
}