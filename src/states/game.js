class Game extends Phaser.State {
    constructor() {
    	super();
    }

    preload() {

        this.game.load.image('assassin-logo', 'assets/images/assassin-logo.png');
        this.game.load.image('mage-logo', 'assets/images/mage-logo.png');
        this.game.load.image('warrior-logo', 'assets/images/warrior-logo.png');

        this.game.load.image('QuestionMark', 'assets/images/QuestionMark.png')

        this.game.load.image('trifecta','assets/images/trifecta.png' )
    }

    create() {
    
        this.firstBTN_A;
        this.firstBTN_B;

        this.background = this.game.add.graphics();
        this.background.beginFill (0x000000);
        this.background.drawRect (0, 0, this.game.width, this.game.height);
        this.background.endFill ();

        this.createClassIcon();
        this.createEnemy();
        this.createTri();

        this.drag = this.createDragText();
        this.text = this.createSelectText();

        game.time.events.add(Phaser.Timer.SECOND * 3, function () {
           this.trio.destroy();
           this.text.destroy();
           
        }, this);
        
    }

    createClassIcon()
    {
        this.icon1 = this.game.add.image(this.game.width * 0.25, 700, "warrior-logo");
        this.icon1.anchor.setTo(0.5, 0.5);
        this.icon1.scale.setTo(1, 1);

        this.icon1.inputEnabled = true;
        this.icon1.input.enableDrag();

        this.icon1 = {
            class : 'warrior'
        }
        console.log(this.icon1.class);
        

        this.icon2 = this.game.add.image(this.game.width * 0.5, 700, "assassin-logo");
        this.icon2.anchor.setTo(0.5, 0.5);
        this.icon2.scale.setTo(1, 1);

        this.icon2.inputEnabled = true;
        this.icon2.input.enableDrag();

        this.icon2 = {
            class : 'assassin'
        }


        this.icon3 = this.game.add.image(this.game.width * 0.75, 700, "mage-logo");
        this.icon3.anchor.setTo(0.5, 0.5);
        this.icon3.scale.setTo(1, 1);

        this.icon3.inputEnabled = true;
        this.icon3.input.enableDrag();

        this.icon3 = {
            class : 'mage'
        }

        
    }

    createEnemy()
    {
    
        this.e_icon1_key = Math.floor(Math.random() * 3) +1;
        this.e_icon1 = this.game.add.image(this.game.width * 0.25, 100, "QuestionMark");
        this.e_icon1.anchor.setTo(0.5, 0.5);
        this.e_icon1.scale.setTo(1, 1);

        if(this.e_icon1_key == 1)
        {
            this.e_icon1 = {
                class : 'warrior'
            }
        }
        else if(this.e_icon1_key == 2)
        {
            this.e_icon1 = {
                class : 'assassin'
            }
        }
        else if(this.e_icon1_key == 3)
        {
            this.e_icon1 = {
                class : 'mage'
            }
        }

        console.log(this.e_icon1.class);
        console.log(this.e_icon1_key);

        this.e_icon2_key = Math.floor(Math.random() * 3) +1;
        this.e_icon2 = this.game.add.image(this.game.width * 0.50, 100, "QuestionMark");
        this.e_icon2.anchor.setTo(0.5, 0.5);
        this.e_icon2.scale.setTo(1, 1);

        if(this.e_icon2_key == 1)
        {
            this.e_icon1 = {
                class : 'warrior'
            }
        }
        else if(this.e_icon2_key == 2)
        {
            this.e_icon1 = {
                class : 'assassin'
            }
        }
        else if(this.e_icon2_key == 3)
        {
            this.e_icon1 = {
                class : 'mage'
            }
        }

        console.log(this.e_icon2_key );

        this.e_icon3_key = Math.floor(Math.random() * 3) +1;
        this.e_icon3 = this.game.add.image(this.game.width * 0.75, 100, "QuestionMark");
        this.e_icon3.anchor.setTo(0.5, 0.5);
        this.e_icon3.scale.setTo(1, 1);

        if(this.e_icon3_key == 1)
        {
            this.e_icon1 = {
                class : 'warrior'
            }
        }
        else if(this.e_icon3_key == 2)
        {
            this.e_icon1 = {
                class : 'assassin'
            }
        }
        else if(this.e_icon3_key == 3)
        {
            this.e_icon1 = {
                class : 'mage'
            }
        }

        console.log(this.e_icon3_key );

    }

    createSelectText()
    {
        
        var style = {
            fontSize: 40,
            font: "Arial",
            fill: "#00ffff",
            align: "center",
            fontWeight: "600",
            stroke: "#0000ff",
            strokeThickness: 2,
            wordWrap: false,
            wordWrapWidth: this.game.width * 0.5
        };

        return this.game.add.text (this.game.width * 0.44, 800, "Select a class", style);
    }

    createDragText()
    {
        
        var style = {
            fontSize: 40,
            font: "Arial",
            fill: "#00ffff",
            align: "center",
            fontWeight: "600",
            stroke: "#0000ff",
            strokeThickness: 2,
            wordWrap: false,
            wordWrapWidth: this.game.width * 0.5
        };

        return this.game.add.text (this.game.width * 0.44, 850, "Drag the icons", style);
    }
    

    createTri()
    {
        this.trio = this.game.add.image(this.game.width * 0.5, this.game.height * 0.5 - 100, "trifecta");
        this.trio.anchor.setTo(0.5, 0.5);
        this.trio.scale.setTo(3, 3);
    }

    

    createButton()
    {

    }


    onButton1()
    {
        this.game.state.start('warrior');
    }

    onButton2()
    {
        this.game.state.start('assassin');
    }

    onButton3()
    {
        this.game.state.start('mage');
    }

    

    update()
    {
        if(this.icon1.position == this.e_icon1.position || 
            this.icon1.position == this.e_icon2.position ||
            this.icon1.position== this.e_icon3.position)
            {
                this.icon1.inputEnabled = false;
                this.icon1.interactable = false;
                
            }
    }
}
