class Preloader extends Phaser.State {
    constructor() {
        super();
    }

    preload() {
        // Disable auto pause on window blur
        if (game && game.stage) {
            game.stage.disableVisibilityChange = true;
            window.document.addEventListener('pause', function() {
                if(!game.paused) {
                    game.isPaused();
                }
            }, false);
            window.document.addEventListener('resume', function() {
                setTimeout(function() {
                    if(game.paused) {
                        game.gameResumed();
                    }
                }, 0);
            }, false);
        }
    }

    create() {
        // add black background
        this.background = this.game.add.graphics();
        this.background.beginFill (0x000000);
        this.background.drawRect (0, 0, this.game.width, this.game.height);
        this.background.endFill ();
        

        var style = {
            font: 'Arial',
            fontSize: 32,
            fill: "#ffffff",
            align: "center"
        };
        
        // add preloader text
        this.titleText = this.game.add.text (this.game.world.centerX, this.game.height * 0.25, game.strings["TEST"], style);
        this.titleText.anchor.setTo (0.5, 0.5);

        // add text for checking if mobile or desktop
        this.mobileText = this.game.add.text (this.game.world.centerX, this.game.height * 0.75, game.isMobile ? game.strings["IS_MOBILE"] : game.strings["IS_DESKTOP"], style);
        this.mobileText.anchor.setTo (0.5, 0.5);
        this.mobileText.fontSize = 20;

        this.createLoadingBar ();
        this.startLoading ();
    }

    createLoadingBar () {
        this.frame = this.game.add.graphics ();
        this.frame.anchor.setTo (0.5, 0.5);
        this.frame.lineStyle(2, 0xffffff, 1);
        this.frame.drawRect(0, 0, game.width * 0.5, game.height * 0.05);
        
        this.bar = this.game.add.graphics ();
        this.bar.anchor.setTo (0.5, 0.5);
        this.bar.beginFill (0x666666, 1);
        this.bar.drawRect(0, 0, game.width * 0.5, game.height * 0.05);
        this.bar.endFill ();

        this.loadingGroup = this.game.add.group ();
        this.loadingGroup.add (this.frame);
        this.loadingGroup.add (this.bar);
        this.loadingGroup.position.setTo (game.width * 0.25, game.height * 0.3);

        this.bar.scale.x = 0;
    }   

    startLoading () {
        this.tweenScale = this.game.add.tween (this.bar.scale).to ({x: 1}, 500, Phaser.Easing.Linear.None, true);
        this.tweenScale.onComplete.addOnce (function () {
            this.game.state.start('gametitle');
        }, this)
    }

    update () {
        
    }
}
