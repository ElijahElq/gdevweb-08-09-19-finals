class GameTitle extends Phaser.State {
    constructor() {
    	super();
    }

    preload() {
        this.game.load.image('ClashOfClass', 'assets/images/ClashOfClass.png');
    }

    create() {

        this.background = this.game.add.graphics();
        this.background.beginFill (0x000000);
        this.background.drawRect (0, 0, this.game.width, this.game.height);
        this.background.endFill ();

        this.createlogo();

        this.text = this.createTitle();
        this.text.position.setTo (this.game.world.centerX, this.game.world.centerY);
        this.text.anchor.setTo(0.5, 0.5);

        this.pressTo = this.toStartText();
        this.pressTo.position.setTo(this.game.world.centerX, this.game.world.centerY + 75);
        this.pressTo.anchor.setTo(0.5, 0.5);
        
    }

    createlogo()
    {
        var logo = this.game.add.image(this.game.world.centerX, this.game.world.centerY, "ClashOfClass");
        logo.anchor.setTo(0.5, 0.5);
        logo.scale.setTo(2, 2);

        this.game.time.events.loop(Phaser.Timer.SECOND, function () {
            logo.tint = Math.random() * 0xffffff;
        }, this);

    }

    createTitle()
    {
        var style = {
            fontSize: 90,
            font: "Arial",
            fill: "#000000",
            align: "center",
            fontWeight: "600",
            stroke: "#00ffff",
            strokeThickness: 2,
            wordWrap: false,
            wordWrapWidth: this.game.width * 0.5
        };

        return this.game.add.text (this.game.width * 0.5, this.game.height * 0., "Clash of class", style);
    }

    toStartText()
    {
        var style = {
            fontSize: 40,
            font: "Arial",
            fill: "#ffffff",
            align: "center",
            fontWeight: "600",
            stroke: "#00ffff",
            strokeThickness: 2,
            wordWrap: false,
            wordWrapWidth: this.game.width * 0.5
        };

        return this.game.add.text (this.game.width * 0.5, this.game.height * 0.6, "Press 'Spacebar' to start", style);
    }

    update(){
        if(game.input.keyboard.isDown(Phaser.Keyboard.SPACEBAR))
        {
            this.game.state.start('game');
        }
    }

    _startMenu () {
    	this.game.state.start('menu');
    }
}
