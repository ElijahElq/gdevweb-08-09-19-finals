class HowToPlay extends Phaser.State {
    constructor() {
    	super();
    }

    preload() {
        
    }

    create() {

        

        this.background = this.game.add.graphics();
        this.background.beginFill (0x000000);
        this.background.drawRect (0, 0, this.game.width, this.game.height);
        this.background.endFill ();

        this.createTutorial();

        this.text = this.createTitle();
        this.text.position.setTo (this.game.world.centerX, this.game.world.centerY);
        this.text.anchor.setTo(0.5, 0.5);

        this.pressTo = this.toStartText();
        this.pressTo.position.setTo(this.game.world.centerX, this.game.world.centerY + 75);
        this.pressTo.anchor.setTo(0.5, 0.5);

        
        
    }

    createTutorial()
    {
        var style = {
            fontSize: 90,
            font: "Arial",
            fill: "#000000",
            align: "center",
            fontWeight: "600",
            stroke: "#00ffff",
            strokeThickness: 2,
            wordWrap: false,
            wordWrapWidth: this.game.width * 0.5
        };

        return this.game.add.text (this.game.width * 0.5, this.game.height * 0., "Clash of class", style);

    }

}
