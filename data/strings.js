strings = {
    "en": {
        "TEST": "Loading",
        "IS_MOBILE": "Testing on Mobile...",
        "IS_DESKTOP": "Testing on Desktop..."
    },
    "fil": {
        "TEST": "Kinakarga",
        "IS_MOBILE": "Sinusubukan sa Cellphone...",
        "IS_DESKTOP": "Sinusubukan sa Kompyuter..."
    }
}